{
    ($input_file): {
        "org.opencontainers.image.title": $output_file,
        "org.opencontainers.image.created": $date,
        "org.opencontainers.image.description": $file_description
    },
    "$config": {
        "org.opencontainers.image.created": $date
    },
    "$manifest": {
        "org.opencontainers.image.title": "jwt_tool",
        "org.opencontainers.image.description": "A toolkit for validating, forging, scanning and tampering JWTs (JSON Web Tokens)",
        "org.opencontainers.image.vendor": "rdnxk",
        "org.opencontainers.image.authors": "rdnxk, San 'rdn' Mônico <san@monico.com.br>",
        "org.opencontainers.image.url": "https://hub.docker.com/r/redemonbr/jwt-tool",
        "org.opencontainers.image.source": "https://gitlab.com/rdnxk/jwt_tool",
        "org.opencontainers.image.documentation": "https://gitlab.com/rdnxk/jwt_tool/-/blob/master/container/docker.md",
        "org.opencontainers.image.created": $date
    }
}
