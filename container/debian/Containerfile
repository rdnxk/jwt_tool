ARG TARGETPLATFORM

FROM docker.io/library/python:3-bookworm

LABEL \
    org.opencontainers.image.authors="rdnxk (https://rdnxk.com), San 'rdn' Mônico <san@rdnxk.com> (https://rdnxk.com)" \
    org.opencontainers.image.description="A toolkit for validating, forging, scanning and tampering JWTs (JSON Web Tokens)" \
    org.opencontainers.image.documentation="https://gitlab.com/rdnxk/jwt_tool/-/blob/main/README.md" \
    org.opencontainers.image.licenses="MIT" \
    org.opencontainers.image.source="https://gitlab.com/rdnxk/jwt_tool" \
    org.opencontainers.image.title="jwt_tool" \
    org.opencontainers.image.url="https://gitlab.com/rdnxk/jwt_tool" \
    org.opencontainers.image.vendor="rdnxk" \
    platform="$TARGETPLATFORM" \
    os="$TARGETOS" \
    arch="$TARGETARCH" \
    variant="$TARGETVARIANT"

WORKDIR /opt/jwt_tool

COPY jwt_tool.py LICENSE requirements.txt README.md ./

RUN \
    echo "TARGETPLATFORM=$TARGETPLATFORM" && \
    apt-get update && \
    apt-get upgrade --yes && \
    pip install --no-cache-dir -r requirements.txt && \
    pip cache purge && \
    rm --recursive --force /var/lib/apt/lists/* && \
    chmod +x jwt_tool.py && \
    ln --symbolic /opt/jwt_tool/jwt_tool.py /usr/local/bin/jwt-tool && \
    ln --symbolic jwt-tool /usr/local/bin/jwt_tool && \
    # generate initial config
    jwt-tool --bare || echo "generated initial config"

ENTRYPOINT ["jwt-tool"]

CMD ["-h"]
